package com.zx.exception;

/**
 * @Project: dataParse
 * @Title: DataParseException
 * @Description: 数据解析异常类
 * @author: zhangxue
 * @date: 2017年11月18日下午3:26:09
 * @version v1.0
 */
public class DataParseException extends Exception {
	
	private static final long serialVersionUID = -8383663535137669207L;
	
	public DataParseException() {
		super();
	}
	
	public DataParseException(String msg) {
		super(msg);
	}
	
	public DataParseException(Exception e, String msg) {
		super(msg, e);
	}
}
