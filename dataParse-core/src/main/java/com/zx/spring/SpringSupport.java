package com.zx.spring;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zx.ParseFactory;
import com.zx.annotation.DataClass;
import com.zx.process.impl.TextDataConfigProcess;
import com.zx.util.ClassUtil;
/**
 * @Project: dataParse
 * @Title: SpringSupport
 * @Description: 为是spring提供的支持类
 * @author: zhangxue
 * @date: 2017年11月18日下午10:20:07
 * @version v1.0
 */
@SuppressWarnings("unused")
public class SpringSupport {
	
	private static Logger logger = LoggerFactory.getLogger(SpringSupport.class);
	
	/** 直接注入类的集合**/
	private List<String> classNameList;
	
	public void setClassNameList(List<String> classNameList) throws Exception {
		for (String classFullName : classNameList) {
			try {
				ParseFactory.register(classFullName);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw e;
			}
		}
	}
	
	/** 直接注入类的集合**/
	private String scanPackage;
	
	public void setScanPackage(String scanPackage) throws Exception {
		List<Class<?>> classes;
		try {
			classes = ClassUtil.getClasses(scanPackage);
			for (Class<?> clazz : classes) {
				DataClass annotation = clazz.getAnnotation(DataClass.class);
				if(annotation != null) {
					String classFullName = clazz.getName();
					ParseFactory.register(classFullName);
				}
			}
		} catch (IOException e ) {
			logger.error("扫描加载类失败", e);
			throw e;
		} catch (ClassNotFoundException e) {
			logger.error("扫描加载类失败", e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
}
