package com.zx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.zx.enums.DataTypeEnum;

/**
 * @Project: dataParse-core
 * @Title: DataClass
 * @Description: 文本格式配置注解(对应一行文本)
 * @author: xue.zhang
 * @date: 2019年1月10日下午5:40:08
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Documented
@Inherited
public @interface DataClass {

	/** 数据编码 */
	String charset() default "UTF-8";
	
	/** 数据类型 */
	DataTypeEnum dataType();
	
	/** 相邻数据之间的间隔符 */
	String split();
	
	/** 文件压缩格式 */
	@Deprecated// TODO
	String compressType() default "rar";
	
	/** 一行数据切割后column的数量,默认是0，不校验 */
	int lineDataLength() default 0;
	
	// ////////////支持高级表达式，定义一行数据解析为一个JavaList///////////////////////
	/** 
	 * arrayInterval为序号的间隔，也就是同种数据在一行数组中索引的差值。
	 * 配合ColumnConfig注解的index字段，自动将符合规律的序号解析为Java对象，最终合并为list
	 * 比如：index=1，arrayInterval=10。
	 * 程序会自动将1,11,21,31...等序号的数字装换为对象的字段，组合成各自的对象，最终合并为list
	 * 默认是0的时候，不开启该功能
	 * @author zhangxue
	 * @date: 2017年12月17日下午10:36:53
	 * */
	int arrayInterval() default 0;
	
	/**
	 * 就是需求解析成集合的时候元素的个数
	 * 如何：index=1，arrayInterval=10，arrayLength=2
	 * 程序会自动将1,11这两个序号的数据封装为对象的字段。
	 * 该字段必须开启arrayInterval（即arrayInterval大于0），arrayLength才能生效
	 * 默认是0的时候，
	 * 		如果arrayInterval开启，则是无限解析下去，直到数组的末尾(保证数组最后需要解析的数据在数组的最后，否则会异常)；
	 * 		如果arrayInterval关闭，该功能关闭。
	 * @author zhangxue
	 * @date: 2017年12月17日下午10:36:53
	 */
	int arrayLength() default 0;
	
}
