package com.zx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Project: dataParse-core
 * @Title: DataField
 * @Description: 文本字段配置注解
 * @author: xue.zhang
 * @date: 2019年1月10日下午5:39:48
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
@Documented
@Inherited
public @interface DataField {
	
	/** 字段描述，比如年龄, 姓名等 */
	String desc() default "";

	/** 这个数据在一行中的位置 */
	int index();
	
	/** 如果是时间格，则设置转换模板 */
	String datePattern() default "yyyy-MM-dd";
	
	/** 是否可以为空，默认是false，不能为空 */
	boolean isNull() default false;
	
	/** 如果数据为空，可以设置默认值(这个值是文本中的值) */
	String defaultValue() default "";
	
	/** 设置字符串最大长度，默认是0，不检验长度 */
	int maxLength() default 0;
	
}
