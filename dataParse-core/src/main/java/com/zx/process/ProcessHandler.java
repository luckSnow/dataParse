package com.zx.process;

import com.zx.model.HandlerArgs;

/**
 * @Project: dataParse-core
 * @Title: ProcessHandler
 * @Description: 处理解析器
 * 		处理器采用责任链模式
 * 			实现类包括两种
 * 				1. class处理器
 * 				2. field处理器
 * @author: xue.zhang
 * @date: 2019年1月14日上午11:30:09
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
public interface ProcessHandler {
	
	/**
	 * @title 当前处理器的处理逻辑
	 * @param target	需要处理的对象
	 * @return			处理成功与否
	 * @author: xue.zhang
	 * @date 2019年1月14日上午11:43:13
	 */
	boolean handler(HandlerArgs target);
	
}
