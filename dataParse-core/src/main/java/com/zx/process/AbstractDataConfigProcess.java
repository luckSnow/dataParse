package com.zx.process;

import java.util.List;

import com.zx.exception.DataParseException;
import com.zx.model.DataClassModel;

/**
 * @Project: dataParse
 * @Title: DataConfigProcess
 * @Description: 文件解析抽象类
 * @author: zhangxue
 * @date: 2017年11月18日下午3:18:45
 * @version v1.0
 */
public abstract class AbstractDataConfigProcess<T> {
	
	/** 目标类的注解对象 */
	private DataClassModel<T> dataConfig;
	
	/* ***********************  1. abstract method    *********************************************/
	
	/**
	 * @Title: 将一行数据解析为数组，并进行校验
	 * @param line		一行文本
	 * @return			返回字段的数组
	 * @throws DataParseException
	 * @date: 2019年1月14日下午10:40:13
	 * @author: zhangxue
	 */
	public abstract String[] splitAndChecked(String line) throws DataParseException;
	
	/* ***********************  1.1 单行解析    *********************************************/

	/**
	 * @Title: 解析一行数据
	 * @param strs			一行文本 DataConfigProcess.splitAndChecked() 后的数组
	 * @return				解析后的对象
	 * @throws Exception 
	 * @date: 2017年11月18日下午3:07:12
	 */
	public abstract T parse(String[] strs) throws DataParseException;
	
	/**
	 * @Title: 解析一行数据
	 * @param line			一行文本
	 * @return				解析后的对象
	 * @throws Exception 
	 * @date: 2017年11月18日下午3:07:12
	 */
	public abstract T parse(String line) throws DataParseException;
	
	/**
	 * @Title: 解析一行数据返回一个集合	
	 * @param strs		一行文本 DataConfigProcess.splitAndChecked() 后的数组
	 * @return	
	 * @throws DataParseException
	 * @author: xue.zhang
	 * @date 2018年2月12日下午4:09:44
	 */
	public abstract List<T> parseList(String[] strs) throws DataParseException;

	/**
	 * @Title: 解析一行数据返回一个集合	
	 * @param strs		一行文本
	 * @return	
	 * @throws DataParseException
	 * @author: xue.zhang
	 * @date 2018年2月12日下午4:09:44
	 */
	public abstract List<T> parseList(String line) throws DataParseException;
	
	/* ***********************  1.2 多行解析    *********************************************/
	/**
	 * @Title: 解析多行数据返回一个集合（一行数据为一个对象，文本较大时不推荐使用）
	 * @param lines
	 * @return	
	 * @throws DataParseException
	 * @author: xue.zhang
	 * @date 2018年2月12日下午4:09:44
	 */
	public abstract List<T> parseList(List<String[]> lines) throws DataParseException;
	
	/**
	 * @Title: 解析多行数据返回一个集合，其中每一行都是一个集合（一行数据为一个集合，文本较大时不推荐使用）	
	 * @param lines
	 * @return	
	 * @throws DataParseException
	 * @author: xue.zhang
	 * @date 2018年2月12日下午4:09:44
	 */
	public abstract List<List<T>> parseListForArrayInterval(List<String[]> lines) throws DataParseException;
	
	/* ***********************  2. common method    *********************************************/
	
	
	/* ***********************  3. getter, setter *********************************************/
	public DataClassModel<T> getDataConfig() {
		return dataConfig;
	}
	
	public void setDataConfig(DataClassModel<T> dataConfig) {
		this.dataConfig = dataConfig;
	}
	
}
