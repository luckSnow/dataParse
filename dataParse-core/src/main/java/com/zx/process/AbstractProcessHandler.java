package com.zx.process;

import com.zx.model.HandlerArgs;

/**
 * @Project: dataParse-core
 * @Title: ProcessHandler
 * @Description: 处理解析器
 * 		处理器采用责任链模式
 * 			实现类包括两种
 * 				1. class处理器
 * 				2. field处理器
 * @author: xue.zhang
 * @date: 2019年1月14日上午11:30:09
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
public abstract class AbstractProcessHandler implements ProcessHandler {

	/**
	 * @title 下一个处理器
	 */
	private ProcessHandler nextHandler;
	
	/**
	 * @title 当前处理器的处理逻辑
	 * @param target	需要处理的对象
	 * @return			处理成功与否
	 * @author: xue.zhang
	 * @date 2019年1月14日上午11:43:13
	 */
	@Override
	public abstract boolean handler(HandlerArgs target);

	/**
	 * @title 执行下一个处理方法
	 * @param target
	 * @return
	 * @author: xue.zhang
	 * @date 2019年1月14日下午1:40:24
	 */
	public boolean handlerNext(HandlerArgs target) {
		if(this.getNextHandler() != null) {
			// 处理 责任链中下一个 处理方法
			return this.getNextHandler().handler(target);
		} else {
			return true;
		}
	}
	
	public ProcessHandler getNextHandler() {
		return nextHandler;
	}

	public void setNextHandler(ProcessHandler nextHandler) {
		this.nextHandler = nextHandler;
	}
	
}
