package com.zx.converter;

import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: Converter
 * @Description: 类型转换器
 * @author: xue.zhang
 * @date: 2019年1月14日上午11:23:01
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
public interface Converter<T> {
	
	/**
	 * @Title: 将object类型转为class类型
	 * @param obj		需要转型的对象
	 * @return			返回目标类型
	 * @date: 2019年1月12日下午10:06:08
	 * @author: zhangxue
	 */
	T convertValue(Object obj) throws DataParseException;
	
	/**
	 * @Title: 获得转换结果的类型
	 * @return
	 * @date: 2019年1月12日下午10:15:17
	 * @author: zhangxue
	 */
	Class<T> getTargetType();
}
