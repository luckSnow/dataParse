package com.zx.converter.base;

import com.zx.converter.Converter;
import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: IntegerConverter
 * @Description: 类型转换器--Float
 * @author: zhangxue
 * @date: 2019年1月12日下午10:24:34
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class FloatConverter implements Converter<Float> {

	@Override
	public Float convertValue(Object obj) throws DataParseException {
		if(obj == null) {
			throw new NullPointerException();
		}
		
		Float result = 0F;
		
		try {
			result = Float.parseFloat(obj.toString());
		} catch (Exception e) {
			throw new DataParseException(e, "类型转换异常: " + obj.toString());
		}
		
		return result;
	}

	@Override
	public Class<Float> getTargetType() {
		return Float.class;
	}

	
}
