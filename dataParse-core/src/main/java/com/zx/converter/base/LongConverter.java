package com.zx.converter.base;

import com.zx.converter.Converter;
import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: IntegerConverter
 * @Description: 类型转换器--long
 * @author: zhangxue
 * @date: 2019年1月12日下午10:24:34
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class LongConverter implements Converter<Long> {

	@Override
	public Long convertValue(Object obj) throws DataParseException {
		if(obj == null) {
			throw new NullPointerException();
		}
		
		Long result = 0L;
		
		try {
			result = Long.parseLong(obj.toString());
		} catch (Exception e) {
			throw new DataParseException(e, "类型转换异常: " + obj.toString());
		}
		
		return result;
	}

	@Override
	public Class<Long> getTargetType() {
		return Long.class;
	}

	
}
