package com.zx.converter.base;

import com.zx.converter.Converter;
import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: IntegerConverter
 * @Description: 类型转换器--Char
 * @author: zhangxue
 * @date: 2019年1月12日下午10:24:34
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class CharConverter implements Converter<Character> {

	@Override
	public Character convertValue(Object obj) throws DataParseException {
		if(obj == null) {
			throw new NullPointerException();
		}
		
		Character result = 0;
		
		try {
			char[] charArray = obj.toString().toCharArray();
			if(charArray == null || charArray.length != 1) {
				throw new DataParseException("类型转换异常,char类型的字符长度必须是1: " + obj.toString());
			}
			result = charArray[0];
		} catch (Exception e) {
			throw new DataParseException(e, "类型转换异常: " + obj.toString());
		}
		
		return result;
	}

	@Override
	public Class<Character> getTargetType() {
		return Character.class;
	}

	
}
