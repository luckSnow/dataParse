package com.zx.converter.reference;

import com.zx.converter.Converter;
import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: IntegerConverter
 * @Description: 引用类型转换器--String
 * @author: zhangxue
 * @date: 2019年1月12日下午10:24:34
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class StringConverter implements Converter<String> {

	@Override
	public String convertValue(Object obj) throws DataParseException {
		if(obj == null) {
			throw new NullPointerException();
		}
		
		return obj.toString();
	}

	@Override
	public Class<String> getTargetType() {
		return String.class;
	}

	
}
