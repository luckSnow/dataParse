package com.zx.converter.reference;


import java.math.BigInteger;

import com.zx.converter.Converter;
import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: IntegerConverter
 * @Description: 引用类型转换器--BigInteger
 * @author: zhangxue
 * @BigInteger: 2019年1月12日下午10:24:34
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class BigIntegerConverter implements Converter<BigInteger> {
	
	@Override
	public BigInteger convertValue(Object obj) throws DataParseException {
		if(obj == null) {
			throw new NullPointerException();
		}
		
		
		BigInteger result = null;
		try {
			result = new BigInteger(obj.toString());
		} catch (Exception e) {
			throw new DataParseException(e, "类型转换异常: " + obj.toString());
		}
		
		return result;
	}

	@Override
	public Class<BigInteger> getTargetType() {
		return BigInteger.class;
	}
}
