package com.zx.reader.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.zx.reader.FileReader;
import com.zx.util.StringUtils;

/**
 * @Project: dataParse-core
 * @Title: TextFileReader
 * @Description: 文本文件读取器
 * 	目前可以保持稳定、良好的性能进行大文本解析
 * @author: xue.zhang
 * @date: 2018年2月12日下午1:45:56
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
public class TextFileReader extends BufferedReader implements FileReader {
	
	/** 默认是UTF-8 */
	private String charset = "UTF-8";
	
	/** 10M缓存 */
	private static int bufferLength = 10 * 1024 * 1024;
	
	/**
	 * @param textFile		文本文件
	 * @param charsetName	文本的编码
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public TextFileReader(File textFile, String charsetName) throws IOException {
		this(new InputStreamReader(new FileInputStream(textFile), charsetName));
		this.charset = charsetName;
	}
	
	/**
	 * @param filePath		带解析的文件路径
	 * @param charsetName	文本的编码
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public TextFileReader(String filePath, String charsetName) throws IOException {
		this(new File(filePath), charsetName);
		this.charset = charsetName;
	}
	
	private TextFileReader(Reader in) {
		super(in, bufferLength);
	}
	
	@Override
	public void close() throws IOException {
		super.close();
	}
	
	/**
	 * 获得一行的文本
	 * @return	返回的结果统一是UTF-8编码的格式
	 * @throws IOException
	 * @author: xue.zhang
	 * @date 2018年2月12日下午2:22:47
	 */
	@Override
	public String readLine() throws IOException{
		return super.readLine();
	}
	
	/**
	 * 获得全部的文本(适用于小文本读取，大文本切记不要使用)
	 * @return	返回的结果统一是UTF-8编码的格式
	 * @throws IOException
	 * @author: xue.zhang
	 * @date 2018年2月12日下午2:22:47
	 */
	public List<String> readLines() throws IOException{
		String line = null;
		List<String> lines = new ArrayList<String>();
		while( (line = this.readLine()) != null) {
			lines.add(line);
		}
		return lines;
	}
	
	/**
	 * @Title: 获得文本的字数
	 * @return
	 * @throws IOException 
	 * @date: 2018年2月17日下午3:01:20
	 */
	public long getWordCount() throws IOException{
		String line = null;
		StringBuffer sb = new StringBuffer();
		while( (line = this.readLine()) != null) {
			sb.append(line);
		}
		return StringUtils.getWordsCount(sb.toString());
	}

	public String getCharset() {
		return this.charset;
	}
}
