package com.zx.reader;

import java.io.Closeable;

/**
 * @Project: dataParse-core
 * @Title: FileReader
 * @Description: 文件读取接口
 * @author: xue.zhang
 * @date: 2018年2月12日下午1:42:10
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
public interface FileReader extends Closeable {

}
