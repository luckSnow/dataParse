package com.zx.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.zx.annotation.DataField;
import com.zx.converter.Converter;
import com.zx.converter.base.BooleanConverter;
import com.zx.converter.base.ByteConverter;
import com.zx.converter.base.CharConverter;
import com.zx.converter.base.DoubleConverter;
import com.zx.converter.base.FloatConverter;
import com.zx.converter.base.IntegerConverter;
import com.zx.converter.base.LongConverter;
import com.zx.converter.base.ShortConverter;
import com.zx.converter.reference.BigDecimalConverter;
import com.zx.converter.reference.BigIntegerConverter;
import com.zx.converter.reference.DateConverter;
import com.zx.converter.reference.StringConverter;
import com.zx.exception.DataParseException;

/**
 * @Project: dataParse-core
 * @Title: ConverterFactory
 * @Description: 格式转换器工厂
 * @author: zhangxue
 * @date: 2019年1月14日下午10:00:46
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class ConverterFactory {
	
	private final static StringConverter STRING_CONVERTER = new StringConverter();
	
	@SuppressWarnings("rawtypes")
	public static Converter getConverter(Field field) throws DataParseException {
		DataField columnConfig = field.getAnnotation(DataField.class);
		if(columnConfig == null) {
			throw new DataParseException("字段" + field + "没有配置字段注解@DataField");
		}
		
		String className = field.getType().getName();
		
		Converter converter = null;
		
		if(String.class.getName().equals(className)) {
			converter = STRING_CONVERTER;
		
		} else if(Integer.class.getName().equals(className) 	|| int.class.getName().equals(className)	){
			converter = new IntegerConverter();
			
		} else if(Long.class.getName().equals(className) 		|| long.class.getName().equals(className)	){
			converter = new LongConverter();
		
		} else if(Double.class.getName().equals(className) 		|| double.class.getName().equals(className)	){
			converter = new DoubleConverter();
		
		} else if(Float.class.getName().equals(className) 		|| float.class.getName().equals(className)	){
			converter = new FloatConverter();
		
		} else if(Boolean.class.getName().equals(className) 	|| boolean.class.getName().equals(className)){
			converter = new BooleanConverter();
		
		} else if(Character.class.getName().equals(className) 	|| char.class.getName().equals(className)	){
			converter = new CharConverter();
		
		} else if(Short.class.getName().equals(className) 		|| short.class.getName().equals(className)	){
			converter = new ShortConverter();
		
		} else if(Byte.class.getName().equals(className) 		|| byte.class.getName().equals(className)	){
			converter = new ByteConverter();
		
		} else if(BigDecimal.class.getName().equals(className)){
			converter = new BigDecimalConverter();
		
		} else if(BigInteger.class.getName().equals(className)){
			converter = new BigIntegerConverter();
		
		} else if(Date.class.getName().equals(className)){
			converter = new DateConverter(columnConfig.datePattern());
			
		} else {
			throw new DataParseException("不支持这种数据类型");
		}
			
		return converter;
	}
	
	
}
