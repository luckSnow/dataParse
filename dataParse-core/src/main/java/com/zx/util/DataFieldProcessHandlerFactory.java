package com.zx.util;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zx.annotation.DataField;
import com.zx.exception.DataParseException;
import com.zx.model.HandlerArgs;
import com.zx.process.AbstractProcessHandler;
import com.zx.process.ProcessHandler;

/**
 * @Project: dataParse-core
 * @Title: DataFieldProcessHandler
 * @Description: 字段--处理器--工厂
 * @author: xue.zhang
 * @date: 2019年1月14日上午11:52:24
 * @company: alibaba
 * @Copyright: Copyright (c) 2017
 * @version v1.0
 */
public class DataFieldProcessHandlerFactory  {
	
	public static ProcessHandler getProcessHandler(Field field) throws DataParseException {
		DataField columnConfig = field.getAnnotation(DataField.class);
		if(columnConfig == null) {
			throw new DataParseException("字段" + field + "没有配置字段注解@DataField");
		}
		// 第一校验
		IsNullCheckedHandler isNullCheckedHandler = new IsNullCheckedHandler(columnConfig.isNull(), columnConfig.defaultValue());
		// 第二校验
		MaxLengthCheckedHandler maxLengthCheckedHandler = new MaxLengthCheckedHandler(columnConfig.maxLength());
		
		isNullCheckedHandler.setNextHandler(maxLengthCheckedHandler);
		
		return isNullCheckedHandler;
	}
	
	/**
	 * @Project: dataParse-core
	 * @Title: IsNullProcessHandler
	 * @Description: 字段null 与 "" 值处理器
	 * 		校验是否可以为空，默认是false，不能为空（校验顺序1）
	 * @author: xue.zhang
	 * @date: 2019年1月14日下午7:46:21
	 * @company: alibaba
	 * @Copyright: Copyright (c) 2017
	 * @version v1.0
	 */
	public static class IsNullCheckedHandler extends AbstractProcessHandler {
		
		private static Logger logger = LoggerFactory.getLogger(IsNullCheckedHandler.class);
		
		private boolean isNull;

		private String defaultValue;
		
		public IsNullCheckedHandler(boolean isNull, String defaultValue) {
			this.isNull = isNull;
			this.defaultValue = defaultValue;
		}

		@Override
		public boolean handler(HandlerArgs args) {
			Object column = args.getObj();
			boolean isNullAndEmpty = column == null || column.toString().length() == 0;
			
			if(isNullAndEmpty) {//字段是空的
				if(!isNull) {//不允许为空
					logger.info("校验失败, 字段不允许为空");
					return false;
				} else {//允许为空，则给默认值
					if(defaultValue != null) {//设置了默认值
						args.setObj(defaultValue);
					} else {
						logger.info("校验失败, 字段为空, 且设置的默认值=null");
						return false;
					}
				}
			}
			return this.handlerNext(args);
		}
	}
	
	/**
	 * @Project: dataParse-core
	 * @Title: MaxLengthProcessHandler
	 * @Description: 校验字符串最大长度，默认是0，不检验长度（校验顺序2）
	 * @author: xue.zhang
	 * @date: 2019年1月14日下午2:48:26
	 * @company: alibaba
	 * @Copyright: Copyright (c) 2017
	 * @version v1.0
	 */
	public static class MaxLengthCheckedHandler extends AbstractProcessHandler {
		
		private static Logger logger = LoggerFactory.getLogger(MaxLengthCheckedHandler.class);
		
		private int maxLength;
		
		public MaxLengthCheckedHandler(int maxLength) {
			this.maxLength = maxLength;
		}
		
		@Override
		public boolean handler(HandlerArgs args) {
			Object column = args.getObj();
			boolean bool = (maxLength == 0) ? true : column.toString().length() <= maxLength;
			// 本次处理未通过，直接返回
			if(bool) {
				return this.handlerNext(args);
			} else {
				logger.info("校验失败, 字符串最大长度={}, 实际={}", maxLength, column.toString().length());
				return false;
			}
		}
	}

}
