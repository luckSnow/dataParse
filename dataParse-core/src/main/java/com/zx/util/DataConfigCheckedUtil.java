package com.zx.util;

import com.zx.model.DataClassModel;
import com.zx.model.DataFieldModel;

public class DataConfigCheckedUtil {
	
	/**
	 * @Title: 一行数据切割后column的数量,默认是0，不校验
	 * @param dataConfig
	 * @param length
	 * @return
	 * @Description: 
	 * @date: 2017年11月18日下午4:34:14
	 */
	public static <T> boolean checkedLineDataLength(DataClassModel<T> dataClassModel, int length) {
		int lineDataLength = dataClassModel.getLineDataLength();
		return (lineDataLength == 0) ?  true : lineDataLength == length;
	}
	
	/**
	 * @Title: 校验是否可以为空，默认是false，不能为空（校验顺序2）
	 * @param columnConfig
	 * @param column
	 * @return
	 * @date: 2017年11月18日下午4:09:05
	 */
	public static boolean checkedIsNull(DataFieldModel columnConfig, String column){
		boolean isNull = columnConfig.getIsNull();
		String defaultValue = columnConfig.getDefaultValue();//默认值
		if(!isNull) {//不允许为空
			boolean isNullAndEmpty = column == null || column.length() == 0;
			if(isNullAndEmpty){//字段真的是空的
				if(defaultValue != null && defaultValue.length() > 0) {
					column = defaultValue;
				} else {
					return false;
				}
			}
			return true;
		} else {
			if(defaultValue != null && defaultValue.length() > 0) {
				column = defaultValue;
			}
			return true;
		}
	}
	
	/**
	 * @Title: 校验字符串最大长度，默认是0，不检验长度（校验顺序3）
	 * @param columnConfig
	 * @param column
	 * @return
	 * @date: 2017年11月18日下午4:09:12
	 */
	public static boolean checkedMaxLength(DataFieldModel columnConfig, String column) {
		int maxLength = columnConfig.getMaxLength();
		return (maxLength == 0) ? true : column.length() <= maxLength;
	}
}
