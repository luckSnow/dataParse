package com.zx.enums;

/**
 * @Project: dataParse
 * @Title: DataType
 * @Description: 文件数据类型
 * @author: zhangxue
 * @date: 2017年11月17日下午9:05:58
 * @version v1.0
 */
public enum DataTypeEnum {
	/** 文本文件 */
	TEXT	("text", 	1),
	/** 压缩格式 */
	BIN		("bin", 	2),
	/** excel */
	EXCEL	("excel", 	3),
	/** word */
	WORD	("word", 	4);
	
	/** 数据类型名称 */
	private String name;
	
	/** 标识*/
	private int index;

	private DataTypeEnum(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}
	
}
