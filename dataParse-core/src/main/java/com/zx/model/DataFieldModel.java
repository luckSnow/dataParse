package com.zx.model;

import java.io.Serializable;
import java.lang.reflect.Field;

import com.zx.converter.Converter;
import com.zx.process.ProcessHandler;

/**
 * @Project: dataParse-core
 * @Title: DataFieldModel
 * @Description: 字段--数据模型
 * @author: zhangxue
 * @date: 2019年1月14日下午10:34:29
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class DataFieldModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 字段的类型 */
	private Field field;
	
	/** 当前字段类型的转换器 */
	@SuppressWarnings("rawtypes")
	private Converter converter;
	
	/** 字段数据校验--处理器 */
	private ProcessHandler checkedHandler;
	
	//////////////以下是注解的配置///////////////////////
	/** 字段描述，比如年龄, 姓名等 */
	private String desc;
	
	/** 这个数据在一行中的位置 */
	private int index;
	
	/** 如果是时间格，则设置转换模板 */
	private String datePattern;
	
	/** 是否可以为空，默认是false，不能为空 */
	private boolean isNull;
	
	/** 如果数据为空，可以设置默认值(这个值是文本中的值) */
	private String defaultValue;
	
	/** 设置字符串最大长度，默认是0，不检验长度 */
	private int maxLength;
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getDatePattern() {
		return datePattern;
	}

	public void setDatePattern(String datePattern) {
		this.datePattern = datePattern;
	}

	public boolean getIsNull() {
		return isNull;
	}

	public void setIsNull(boolean isNull) {
		this.isNull = isNull;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	@SuppressWarnings("rawtypes")
	public Converter getConverter() {
		return converter;
	}

	@SuppressWarnings("rawtypes")
	public void setConverter(Converter converter) {
		this.converter = converter;
	}

	public ProcessHandler getCheckedHandler() {
		return checkedHandler;
	}

	public void setCheckedHandler(ProcessHandler checkedHandler) {
		this.checkedHandler = checkedHandler;
	}

	@Override
	public String toString() {
		return "DataFieldModel [field=" + field + ", converter=" + converter
				+ ", checkedHandler=" + checkedHandler + ", desc=" + desc
				+ ", index=" + index + ", datePattern=" + datePattern
				+ ", isNull=" + isNull + ", defaultValue=" + defaultValue
				+ ", maxLength=" + maxLength + "]";
	}
	
}
