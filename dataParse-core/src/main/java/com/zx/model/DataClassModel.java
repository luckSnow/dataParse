package com.zx.model;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zx.enums.DataTypeEnum;
import com.zx.process.ProcessHandler;

/**
 * @Project: dataParse-core
 * @Title: DataClassModel
 * @Description: 类(行数据)--数据模型
 * @author: zhangxue
 * @date: 2019年1月14日下午10:34:29
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class DataClassModel<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 注册的类 */
	private Class<T> registerClass;
	
	/** 行数据校验--处理器 */
	private ProcessHandler checkedHandler;
	
	//////////////以下是注解的配置///////////////////////
	
	/** 数据编码 */
	private String charset;
	
	/** 数据类型 */
	private DataTypeEnum dataType;
	
	/** 相邻数据之间的间隔符 */
	private String split;
	
	/** 一行数据切割后column的数量,默认是0，不校验 */
	private int lineDataLength;
	
	//////////////支持高级表达式，定义一行数据解析为一个JavaList///////////////////////
	/**  arrayInterval为序号的间隔 **/
	private int arrayInterval;
	
	/** 就是需求解析成集合的时候元素的个数 */
	private int arrayLength;
	
	/** 字段的配置 
	 * key=DataFieldModel中的index
	 * value=DataFieldModel
	 **/
	private final Map<Integer, DataFieldModel> fieldMap = new ConcurrentHashMap<Integer, DataFieldModel>();

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public DataTypeEnum getDataType() {
		return dataType;
	}

	public void setDataType(DataTypeEnum dataType) {
		this.dataType = dataType;
	}

	public String getSplit() {
		return split;
	}

	public void setSplit(String split) {
		this.split = split;
	}

	public int getLineDataLength() {
		return lineDataLength;
	}

	public void setLineDataLength(int lineDataLength) {
		this.lineDataLength = lineDataLength;
	}

	public int getArrayInterval() {
		return arrayInterval;
	}

	public void setArrayInterval(int arrayInterval) {
		this.arrayInterval = arrayInterval;
	}

	public int getArrayLength() {
		return arrayLength;
	}

	public void setArrayLength(int arrayLength) {
		this.arrayLength = arrayLength;
	}

	public Map<Integer, DataFieldModel> getFieldMap() {
		return fieldMap;
	}

	public Class<T> getRegisterClass() {
		return registerClass;
	}

	public void setRegisterClass(Class<T> registerClass) {
		this.registerClass = registerClass;
	}

	public ProcessHandler getCheckedHandler() {
		return checkedHandler;
	}

	public void setCheckedHandler(ProcessHandler checkedHandler) {
		this.checkedHandler = checkedHandler;
	}

	@Override
	public String toString() {
		return "DataClassModel [registerClass=" + registerClass
				+ ", checkedHandler=" + checkedHandler + ", charset=" + charset
				+ ", dataType=" + dataType + ", split=" + split
				+ ", lineDataLength=" + lineDataLength + ", arrayInterval="
				+ arrayInterval + ", arrayLength=" + arrayLength + ", fieldMap="
				+ fieldMap + "]";
	}
	
}
