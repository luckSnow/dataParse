package com.zx.model;

public class HandlerArgs {
	private Object obj;

	public HandlerArgs() {
		// TODO Auto-generated constructor stub
	}
	
	
	public HandlerArgs(Object obj) {
		super();
		this.obj = obj;
	}


	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
	
}
