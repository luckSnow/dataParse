package com.zx.test.validate;

import java.text.ParseException;

import org.junit.Assert;
import org.junit.Test;

import com.zx.ParseFactory;
import com.zx.exception.DataParseException;
import com.zx.process.AbstractDataConfigProcess;

/**
 * @Project: dataParse-test
 * @Title: ValidateTest
 * @Description: 配置校验--测试类
 * @author: zhangxue
 * @date: 2019年1月16日下午10:57:12
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class ValidateTest {
	
	@Test
	public void test1() throws DataParseException, ParseException {
		AbstractDataConfigProcess<VUser_1> register = ParseFactory.register(VUser_1.class);
		String str1 = "1#张三#1900-01-01#20#男";//正常字段
		// 长度校验
		String[] splitAndChecked = register.splitAndChecked(str1);
		Assert.assertEquals(splitAndChecked.length, 5);
		
		// 名字为空（默认不能为空，否则异常）
		String str2 = "1##1900-01-01#20#男";
		String[] ss2 = register.splitAndChecked(str2);
		try {
			register.parse(ss2);
		} catch (Exception e) {//异常时正常的情况
			Assert.assertEquals(true, true);
		}
		
		//时间为空（设置为可以为空，并给默认值）
		String str3 = "1#张三##20#男";
		String[] ss3 = register.splitAndChecked(str3);
		VUser_1 parse = register.parse(ss3);
		System.out.println(parse);
		
		//年龄为空（设置为可以为空，不给默认值，由于默认值为空，则由于类型转化的时候异常）
		String str4 = "1#张三#1900-01-01##男";
		String[] ss4 = register.splitAndChecked(str4);
		
		try {
			register.parse(ss4);
		} catch (Exception e) {
			Assert.assertEquals(true, true);
		}

		//性别为空（设置为可以为空，不给默认值）
		String str5 = "1#张三#1900-01-01#20#";
		String[] ss5 = register.splitAndChecked(str5);
		VUser_1 parse5 = register.parse(ss5);
		System.out.println(parse5);
		
	}
	
}
