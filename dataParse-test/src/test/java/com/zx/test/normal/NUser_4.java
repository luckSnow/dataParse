package com.zx.test.normal;

import java.util.Date;

import com.zx.annotation.DataField;
import com.zx.annotation.DataClass;
import com.zx.enums.DataTypeEnum;

@DataClass(dataType = DataTypeEnum.TEXT, split = "\\|#\\|", charset = "UTF-8", arrayInterval=3, arrayLength=3)
public class NUser_4 {
	
	@DataField(index = 0)
	private Integer id;
	
	@DataField(index = 1)
	private String name;
	
	@DataField(index = 2, datePattern = "yyyy-MM-dd")
	private Date brithday;
	
	private int age;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBrithday() {
		return brithday;
	}

	public void setBrithday(Date brithday) {
		this.brithday = brithday;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "NUser_4 [id=" + id + ", name=" + name + ", brithday=" + brithday
				+ ", age=" + age + "]";
	}
	
}
