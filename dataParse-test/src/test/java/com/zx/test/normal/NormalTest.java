package com.zx.test.normal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.zx.ParseFactory;
import com.zx.exception.DataParseException;
import com.zx.process.AbstractDataConfigProcess;


/**
 * @Project: dataParse-test
 * @Title: NormalTest
 * @Description: 文本解析--测试类--简单文本
 * @author: zhangxue
 * @date: 2019年1月15日下午10:42:59
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class NormalTest {
	
	// 简单分隔符
	@Test
	public void test1() throws DataParseException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		AbstractDataConfigProcess<NUser_1> register = ParseFactory.register(NUser_1.class);
		String str1 = "1 张三 1900-01-01";
		String[] splitAndChecked = register.splitAndChecked(str1);
		NUser_1 parse = register.parse(splitAndChecked);
		
		Assert.assertEquals(parse.getId(), 			new Integer(1)			);
		Assert.assertEquals(parse.getName(), 		"张三"					);
		Assert.assertEquals(parse.getBrithday(), 	sdf.parse("1900-01-01")	);
		
	}
	// 复杂分隔符
	@Test
	public void test2() throws DataParseException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		AbstractDataConfigProcess<NUser_2> register = ParseFactory.register(NUser_2.class);
		String str1 = "1|#|张三|#|1900-01-01";
		String[] splitAndChecked = register.splitAndChecked(str1);
		NUser_2 parse = register.parse(splitAndChecked);
		
		Assert.assertEquals(parse.getId(), 			new Integer(1)			);
		Assert.assertEquals(parse.getName(), 		"张三"					);
		Assert.assertEquals(parse.getBrithday(), 	sdf.parse("1900-01-01")	);
		
	}
	// 复杂分隔符--字段空值
	@Test
	public void test3() throws DataParseException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		AbstractDataConfigProcess<NUser_3> register = ParseFactory.register(NUser_3.class);
		String str1 = "1|#|张三|#||#|1900-01-01";
		String[] splitAndChecked = register.splitAndChecked(str1);
		NUser_3 parse = register.parse(splitAndChecked);
		
		Assert.assertEquals(parse.getId(), 			new Integer(1)			);
		Assert.assertEquals(parse.getName(), 		"张三"					);
		Assert.assertEquals(parse.getBrithday(), 	sdf.parse("1900-01-01")	);
		
	}
	// 复杂分隔符--集合--指定集合元素数量
	@Test
	public void test4() throws DataParseException, ParseException {
		AbstractDataConfigProcess<NUser_4> register = ParseFactory.register(NUser_4.class);
		String str1 = "1|#|张三|#|1901-01-01|#|2|#|李四|#|1902-01-01|#|3|#|王五|#|1903-01-01";
		String[] splitAndChecked = register.splitAndChecked(str1);
		List<NUser_4> parseList = register.parseList(splitAndChecked);
		for (NUser_4 nUser_4 : parseList) {
			System.out.println(nUser_4);
			Assert.assertNotEquals(nUser_4.getId(), 				null	);
			Assert.assertNotEquals(nUser_4.getName(), 				null	);
			Assert.assertNotEquals(nUser_4.getBrithday(), 			null	);
		}	
	}
	
	// 复杂分隔符--集合--没有指定集合元素数量
	@Test
	public void test5() throws DataParseException, ParseException {
		AbstractDataConfigProcess<NUser_5> register = ParseFactory.register(NUser_5.class);
		String str1 = "1|#|张三|#|1901-01-01|#|2|#|李四|#|1902-01-01|#|3|#|王五|#|1903-01-01|#|4|#|赵六|#|1904-01-01";
		String[] splitAndChecked = register.splitAndChecked(str1);
		List<NUser_5> parseList = register.parseList(splitAndChecked);
		for (NUser_5 nUser_5 : parseList) {
			System.out.println(nUser_5);
			Assert.assertNotEquals(nUser_5.getId(), 				null	);
			Assert.assertNotEquals(nUser_5.getName(), 				null	);
			Assert.assertNotEquals(nUser_5.getBrithday(), 			null	);
		}	
	}
	
}
