package com.zx.test.spring;

import java.util.Date;

import com.zx.annotation.DataField;
import com.zx.annotation.DataClass;
import com.zx.enums.DataTypeEnum;

@DataClass(dataType = DataTypeEnum.TEXT, split = "#", charset = "UTF-8", lineDataLength = 5)
public class SUser_1 {
	
	@DataField(index = 0)
	private Integer id;
	
	//默认不允许为空，不能设置默认值。数据为空时会抛出异常
	@DataField(index = 1, isNull = false)
	private String name;
	
	// 允许为空，空的时候给默认值，正常解析
	@DataField(index = 2, datePattern = "yyyy-MM-dd", isNull = true, defaultValue="1990-01-01")
	private Date brithday;
	
	// 允许为空，不设置默认值
	@DataField(index = 3, isNull = true)
	private int age;

	// 允许为空，不设置默认值
	@DataField(index = 4, isNull = true)
	private String sex;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBrithday() {
		return brithday;
	}

	public void setBrithday(Date brithday) {
		this.brithday = brithday;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "VUser_1 [id=" + id + ", name=" + name + ", brithday=" + brithday
				+ ", age=" + age + ", sex=" + sex + "]";
	}
	
}
