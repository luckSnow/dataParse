package com.zx.test.spring;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zx.ParseFactory;
import com.zx.exception.DataParseException;
import com.zx.process.AbstractDataConfigProcess;
import com.zx.test.validate.VUser_1;

public class SpringTest {
	@SuppressWarnings("resource")
	@Test
	public void test() throws DataParseException {
		ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
		app.start();
		
		AbstractDataConfigProcess<VUser_1> register = ParseFactory.register(VUser_1.class);
		String str = "1#张三#1900-01-01#20#男";//正常字段
		
		VUser_1 parse = register.parse(str);
		System.out.println(parse);
		
	}
}
