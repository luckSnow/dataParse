package com.zx.test.reader;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.zx.reader.impl.ExcelFileReader;

public class ExcelReadTest {
	
	@Test
	public void test1() throws Exception {
		ExcelFileReader reader = new ExcelFileReader("src/test/java/data/a.xls");
		//按照sheet名字获取数据
		List<String[]> read = reader.read("Sheet3");
		for (String[] s : read) {
			System.out.println(Arrays.toString(s));
		}
	}
	
	@Test
	public void test2() throws Exception {
		ExcelFileReader reader = new ExcelFileReader("src/test/java/data/a.xls");
		//按照sheet页索引获取数据
		List<String[]> read = reader.read(4);
		for (String[] s : read) {
			System.out.println(Arrays.toString(s));
		}
	}
}
