package com.zx.test.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.zx.ParseFactory;
import com.zx.exception.DataParseException;
import com.zx.process.AbstractDataConfigProcess;
import com.zx.reader.impl.TextFileReader;
import com.zx.reader.impl.WordFileReader;

public class FileReaderTest {
	
	/**
	 * @Title: 解析utf8文件
	 * @throws Exception
	 * @date: 2019年1月18日下午11:06:10
	 * @author: zhangxue
	 */
	@Test
	public void test1() throws Exception {
		// 注册类的解析器，自动返回一个解析器
		AbstractDataConfigProcess<RUser_1> dataConfigProcess = ParseFactory.register(RUser_1.class);
		String filePath = "src/test/java/data/utf8.txt";
		// 读取小文件
		TextFileReader reader = new TextFileReader(filePath, dataConfigProcess.getDataConfig().getCharset());
		// 获得所有行
		List<String> readLines = reader.readLines();
		// 遍历解析
		for (String str : readLines) {
			try {
				RUser_1 parse = dataConfigProcess.parse(str);
				System.out.println(parse);
			} catch (DataParseException e) {
				e.printStackTrace();
			}
		}
		System.out.println("###################################");
		// 直接解析全部数据
		try {
			List<String[]> lls = new ArrayList<String[]>(readLines.size());
			for (String string : readLines) {
				lls.add(dataConfigProcess.splitAndChecked(string));
			}
			List<RUser_1> parseList = dataConfigProcess.parseList(lls);
			System.out.println(parseList);
		} catch (DataParseException e) {
			e.printStackTrace();
		}
		// 关闭资源
		reader.close();
	}
	
	/**
	 * @Title: 解析gb2312文件
	 * @throws Exception
	 * @date: 2019年1月18日下午11:06:10
	 * @author: zhangxue
	 */
	@Test
	public void test2() throws Exception {
		// 注册类的解析器，自动返回一个解析器
		AbstractDataConfigProcess<RUser_2> dataConfigProcess = ParseFactory.register(RUser_2.class);
		String filePath = "src/test/java/data/gb2312.txt";
		// 读取小文件
		TextFileReader reader = new TextFileReader(filePath, dataConfigProcess.getDataConfig().getCharset());
		// 获得所有行
		List<String> readLines = reader.readLines();
		// 遍历解析
		for (String str : readLines) {
			try {
				RUser_2 parse = dataConfigProcess.parse(str);
				System.out.println(parse);
			} catch (DataParseException e) {
				e.printStackTrace();
			}
		}
		System.out.println("###################################");
		// 直接解析全部数据
		try {
			List<String[]> lls = new ArrayList<String[]>(readLines.size());
			for (String string : readLines) {
				lls.add(dataConfigProcess.splitAndChecked(string));
			}
			List<RUser_2> parseList = dataConfigProcess.parseList(lls);
			System.out.println(parseList);
		} catch (DataParseException e) {
			e.printStackTrace();
		}
		// 关闭资源
		reader.close();
	}
	
	
	/**
	 * 读取文办格式文件和word文件内容字数
	 * @throws IOException
	 * @author: xue.zhang
	 * @date 2018年5月21日下午2:35:35
	 */
	@Test
	public void test3() throws IOException{
		String[] ps = {"src/test/java/data"};
		
		for (String s : ps) {
			long sum1 = getCount(s);
			System.out.println(s + ":  " + sum1);
		}
		
//		TextFileReader reader = new TextFileReader("F://集合介绍.txt", "utf-8");
//		long wordCount = reader.getWordCount();
//		System.out.println(wordCount);
	}
	
	
	
	
	public static long getCount(String filePath) throws IOException{
		Long sum = 0L;
		File file = new File(filePath);
		// 跳过隐藏文件
		if(file.getName().startsWith(".") || file.getName().startsWith("$")){
			return sum;
		}
		if(file.isDirectory()){
			File[] listFiles = file.listFiles();
			if(listFiles != null && listFiles.length > 0) {
				for (File file2 : listFiles) {
					sum += getCount(file2.getPath());
				}
			}
		} else {
			filePath = filePath.toLowerCase();
			if(filePath.endsWith(".txt") || filePath.endsWith(".md")) {
				TextFileReader reader = new TextFileReader(file, "utf-8");
				long wordCount = reader.getWordCount();
				System.out.println(filePath + "\t\t" + wordCount);
				sum += wordCount;
				reader.close();
			} else if (filePath.endsWith(".doc") || filePath.endsWith(".docx")){
				WordFileReader reader = new WordFileReader(filePath);
				long wordCount = reader.getWordCount();
				System.out.println(filePath + "\t\t" + wordCount);
				sum += wordCount; 
			}
		}
		return sum;
		
	}
}
