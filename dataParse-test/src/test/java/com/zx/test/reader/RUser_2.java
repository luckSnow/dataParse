package com.zx.test.reader;

import java.util.Date;

import com.zx.annotation.DataField;
import com.zx.annotation.DataClass;
import com.zx.enums.DataTypeEnum;

/**
 * @Project: dataParse-test
 * @Title: PUser
 * @Description: 性能测试--用户基本信息
 * @author: zhangxue
 * @date: 2019年1月13日上午11:18:51
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
@DataClass(dataType = DataTypeEnum.TEXT, split = "#", charset = "GB2312")
public class RUser_2 {
	
	@DataField(desc = "编号", index = 0)
	private Integer id;
	
	@DataField(desc = "姓名", index = 1)
	private String name;
	
	@DataField(desc = "出生日期", index = 2, datePattern = "yyyy-MM-dd", defaultValue = "1900-01-01")
	private Date brithday;

	@DataField(desc = "性别", index = 3)
	private String male;

	@DataField(desc = "字段1", index = 4)
	private String field1;
	
	@DataField(desc = "字段2", index = 5)
	private String field2;
	
	@DataField(desc = "字段3", index = 6)
	private String field3;
	
	@DataField(desc = "字段4", index = 7)
	private String field4;
	
	@DataField(desc = "字段5", index = 8)
	private String field5;

	public RUser_2() {	}
	
	public RUser_2(Integer id, String name, Date brithday, String male,
			String field1, String field2, String field3, String field4,
			String field5) {
		super();
		this.id = id;
		this.name = name;
		this.brithday = brithday;
		this.male = male;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
		this.field4 = field4;
		this.field5 = field5;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBrithday() {
		return brithday;
	}

	public void setBrithday(Date brithday) {
		this.brithday = brithday;
	}

	public String getMale() {
		return male;
	}

	public void setMale(String male) {
		this.male = male;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	@Override
	public String toString() {
		return id + "#" + name + "#" + brithday
				+ "#" + male + "#" + field1 + "#" + field2
				+ "#" + field3 + "#" + field4 + "#"
				+ field5;
	}
	
}
