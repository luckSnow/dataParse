package com.zx.test.util;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.zx.exception.DataParseException;
import com.zx.process.ProcessHandler;
import com.zx.util.ClassUtil;

public class ClassUtilTest {

	@Test
	public void testNewInstance() throws DataParseException {
		Date newInstance = ClassUtil.newInstance(Date.class); 
		System.out.println(newInstance);
	}

	@Test
	public void testGetAllClassByInterface() throws ClassNotFoundException, IOException {
		List<Class> allClassByInterface = ClassUtil.getAllClassByInterface(ProcessHandler.class);
		for (Class<?> class1 : allClassByInterface) {
			System.out.println(class1);
		}
	}

	@Test
	public void testGetClasses() {
		try {
			List<Class<?>> classes = ClassUtil.getClasses("com.zx");
			for (Class<?> class1 : classes) {
				System.out.println(class1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetClasses1() {
		try {
			List<Class<?>> classes = ClassUtil.getClasses("org.junit.runners");
			for (Class<?> class1 : classes) {
				System.out.println(class1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
