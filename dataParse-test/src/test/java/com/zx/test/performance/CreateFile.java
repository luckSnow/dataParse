package com.zx.test.performance;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @Project: dataParse-test
 * @Title: CreateFile
 * @Description: 性能测试--生成测试文件
 * @author: zhangxue
 * @date: 2019年1月13日上午11:40:40
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class CreateFile {
	public static void main(String[] args) throws IOException {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String brithday = sdf.format(date);
		
		Random random = new Random();
		
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("D://data.txt")), "GBK"));
		
		long begin = System.currentTimeMillis();
		
		long l1 = System.currentTimeMillis();
		int length = 1000;
//		int length = 1000 * 1000;
		for (int i = 0; i < length; i++) {
			
			PUser user = new PUser(i, "张三", date, "男", "111", "222", "333", "444", "555");
			PGrade grade1 = new PGrade("语文", 99.5D, 1);
			PGrade grade2 = new PGrade("数学", 60D, 2);
			PGrade grade3 = new PGrade("英语", 70D, 3);
			PGrade grade4 = new PGrade("物理", 60D, 4);
			PGrade grade5 = new PGrade("化学", 100D, 5);
			
			String line = user.toString() 
					+ "#" + grade1.toString()
					+ "#" + grade2.toString()
					+ "#" + grade3.toString()
					+ "#" + grade4.toString()
					+ "#" + grade5.toString()
					+ "\r\n";
			
			// 替换字段值
			String[] split = line.split("#");
			split[2] = brithday;
			String lineStr = "";
			
			// 将其中某个字段置为空。
			if(i % 10 == 0) {
				int idx = random.nextInt(split.length - 3) + 2;
				split[idx] = "";
			}
			
			// 拼接结果
			for (int j = 0; j < split.length; j++) {
				if(j == split.length - 1) {
					lineStr += split[j];
				} else {
					lineStr += split[j] + "#";
				}
			}
			writer.write(lineStr);
			
			if(i % (1000 * 10) == 0) {
				long l2 = System.currentTimeMillis();
				int ms = (int) (l2 - l1);
				System.out.println("耗时： " + ms + " ms");
				l1 = l2;
			}
		}
		
		writer.close();
		
		long end = System.currentTimeMillis();
		int second = (int) (end - begin) / 1000;
		System.out.println("共耗时： " + second + " s");
		
	}
}
