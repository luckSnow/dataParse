package com.zx.test.performance;


import com.zx.annotation.DataClass;
import com.zx.annotation.DataField;
import com.zx.enums.DataTypeEnum;

/**
 * @Project: dataParse-test
 * @Title: PGrade
 * @Description: 性能测试--成绩单
 * @author: zhangxue
 * @date: 2019年1月13日上午11:28:44
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
@DataClass(dataType = DataTypeEnum.TEXT, split = "#", charset = "UTF-8")
public class PGrade {
	
	@DataField(desc = "学科名称", index = 9)
	private String gradeName;
	
	@DataField(desc = "分数", index = 10)
	private Double grade;
	
	@DataField(desc = "排序", index = 11)
	private Integer order;

	public PGrade(String gradeName, Double grade, Integer order) {
		super();
		this.gradeName = gradeName;
		this.grade = grade;
		this.order = order;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public Double getGrade() {
		return grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return gradeName + "#" + grade + "#" + order;
	}
	
}
