package com.zx.test.converter;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.zx.converter.Converter;
import com.zx.converter.base.BooleanConverter;
import com.zx.converter.base.ByteConverter;
import com.zx.converter.base.CharConverter;
import com.zx.converter.base.DoubleConverter;
import com.zx.converter.base.FloatConverter;
import com.zx.converter.base.IntegerConverter;
import com.zx.converter.base.LongConverter;
import com.zx.converter.base.ShortConverter;
import com.zx.converter.reference.BigDecimalConverter;
import com.zx.converter.reference.BigIntegerConverter;
import com.zx.converter.reference.DateConverter;
import com.zx.converter.reference.StringConverter;
import com.zx.exception.DataParseException;


/**
 * @Project: dataParse-test
 * @Title: ConverterTest
 * @Description: 类型转换器--测试类
 * @author: zhangxue
 * @date: 2019年1月15日下午9:44:53
 * @company: 未知之地
 * @Copyright: Copyright (c) 2019
 * @version v1.0
 */
public class ConverterTest {

	@Test
	public void testBooleanConverter() throws DataParseException {
		Converter<Boolean> converter = new BooleanConverter();
		
		Assert.assertEquals( converter.convertValue("true"), Boolean.TRUE);
		Assert.assertEquals( converter.convertValue("false"), Boolean.FALSE);
		
		Assert.assertEquals( converter.convertValue("TRUE"), Boolean.TRUE);
		Assert.assertEquals( converter.convertValue("FALSE"), Boolean.FALSE);
	}

	@Test
	public void testByteConverter() throws DataParseException {
		Converter<Byte> converter = new ByteConverter();
		Byte a = 1;
		Assert.assertEquals( converter.convertValue("1"), a);
	}

	@Test
	public void testCharConverter() throws DataParseException {
		Converter<Character> converter = new CharConverter();
		Character a = '1';
		Assert.assertEquals( converter.convertValue("1"), a);
	}
	
	@Test
	public void testDoubleConverter() throws DataParseException {
		Converter<Double> converter = new DoubleConverter();
		Double a = 1.1D;
		Assert.assertEquals( converter.convertValue("1.1"), a);
	}
	
	@Test
	public void testFloatConverter() throws DataParseException {
		Converter<Float> converter = new FloatConverter();
		Float a = 1.1F;
		Assert.assertEquals( converter.convertValue("1.1"), a);
	}
	
	@Test
	public void testIntegerConverter() throws DataParseException {
		Converter<Integer> converter = new IntegerConverter();
		Integer a = 1;
		Assert.assertEquals( converter.convertValue("1"), a);
	}
	
	@Test
	public void testLongConverter() throws DataParseException {
		Converter<Long> converter = new LongConverter();
		Long a = 1L;
		Assert.assertEquals( converter.convertValue("1"), a);
	}
	
	@Test
	public void testShortConverter() throws DataParseException {
		Converter<Short> converter = new ShortConverter();
		Short a = 1;
		Assert.assertEquals( converter.convertValue("1"), a);
	}
	
	@Test
	public void testStringConverter() throws DataParseException {
		Converter<String> converter = new StringConverter();
		Assert.assertEquals( converter.convertValue("abc"), "abc");
	}
	
	@Test
	public void testDateConverter() throws DataParseException {
		Converter<Date> converter = new DateConverter("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String str = sdf.format(date);
		
		Date convertValue = converter.convertValue(str);
		String str2 = sdf.format(convertValue);
		
		Assert.assertEquals(str, str2);
	}
	
	@Test
	public void testBigIntegerConverter() throws DataParseException {
		Converter<BigInteger> converter = new BigIntegerConverter();
		
		BigInteger bigInteger = converter.convertValue("1");
		Assert.assertEquals(bigInteger, new BigInteger("1"));
	}
	
	@Test
	public void testBigDecimalConverter() throws DataParseException {
		Converter<BigDecimal> converter = new BigDecimalConverter();
		
		BigDecimal bigInteger = converter.convertValue("1");
		Assert.assertEquals(bigInteger, new BigDecimal("1"));
	}
	
	

}
