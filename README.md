在实际开发中常遇到文本类文件的解析，并且文本格式内容经常变化。本项目主要用于解决这种场景 

# 第一章 快速入门
- JDK 1.5+

## 一、简单使用
	
从一行数据中提取一个对象
	
> 1.文件

比如一个文件是“data.txt”
里面内容的格式是：

	1 张三 1901-01-01
	2 李四 1902-01-01

> 2.实体类

```java
@DataClass(dataType = DataTypeEnum.TEXT, split = " ", charset = "UTF-8")
public class NUser_1 {

	@DataField(index = 0)
	private Integer id;
	
	@DataField(index = 1)
	private String name;
	
	@DataField(index = 2, datePattern = "yyyy-MM-dd")
	private Date brithday;
	
	private int age;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBrithday() {
		return brithday;
	}

	public void setBrithday(Date brithday) {
		this.brithday = brithday;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
```
	
	
> 3.直接使用
```java
@Test
public void test1() throws DataParseException, ParseException {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	DataConfigProcess<NUser_1> register = ParseFactory.register(NUser_1.class);
	String str1 = "1 张三 1900-01-01";
	String[] splitAndChecked = register.splitAndChecked(str1);
	NUser_1 parse = register.parse(splitAndChecked);
	
	Assert.assertEquals(parse.getId(), 			new Integer(1)			);
	Assert.assertEquals(parse.getName(), 		"张三"					);
	Assert.assertEquals(parse.getBrithday(), 	sdf.parse("1900-01-01")	);
	
}
```	
	
> 4.spring 支持
		
>> 4.1 spring 配置
```xml
	<!-- 方式1：注入集合类 -->
	<bean id="ss" class="com.zx.spring.SpringSupport">
		<property name="classNameList">
			<list>
				<value>com.zx.test.User</value>
			</list>
		</property>
	</bean>
	
	<!-- 方式2：扫描包 -->
	<bean id="ss" class="com.zx.spring.SpringSupport">
		<property name="scanPackage" value="com.zx.test"></property>
	</bean>
```
	
>> 4.2 spring使用
```java
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		String str = "1111 张三 2017-01-01";
		ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
		app.start();
		try {
			User user = ParseFactory.getMapper(User.class).parse(str);
			System.out.println(user);
		} catch (DataParseException e) {
			e.printStackTrace();
		}
	}
```

> 5. spring boot 支持

使用下面的方式注册后，使用方式还是使用ParseFactory获得解析器

```java
@Configuration
public class Config {
	
	/**
	 * @Title: spring boot 注册方式1
	 * @return
	 * @throws Exception
	 * @date: 2019年1月19日下午10:54:27
	 * @author: zhangxue
	 */
	@Bean
    public SpringSupport register() throws Exception {
        SpringSupport support = new SpringSupport();
        support.setScanPackage("com.zx");
        return support;
    }
	
	/**
	 * @Title: spring boot 注册方式2
	 * @return
	 * @throws Exception
	 * @date: 2019年1月19日下午10:54:27
	 * @author: zhangxue
	 */
	@Bean
	public SpringSupport register1() throws Exception {
		List<String> list = Arrays.asList(
				NUser_1.class.getName());
		
		SpringSupport support = new SpringSupport();
		support.setClassNameList(list);
		return support;
	}
}
```
## 二、集合应用
	
从一行数据中提取一个JavaList
	
> 1.文件

比如一个文件是“data.txt”
里面内容的格式是：
“张三 2017-01-01 李四 2017-02-02 王五 2017-02-02 111 2017”
		

> 2.实体类
	
arrayInterval为序号的间隔，也就是同种数据在一行数组中索引的差值。
arrayInterval是需求解析成集合的时候元素的个数
	
```java
@DataClass(dataType = DataTypeEnum.TEXT, split = "\\|#\\|", charset = "UTF-8", arrayInterval=3, arrayLength=3)
public class NUser_4 {
	
	@DataField(index = 0)
	private Integer id;
	
	@DataField(index = 1)
	private String name;
	
	@DataField(index = 2, datePattern = "yyyy-MM-dd")
	private Date brithday;
	
	private int age;
	
	....
}
```	
> 3.直接使用

```java
@Test
public void test4() throws DataParseException, ParseException {
	DataConfigProcess<NUser_4> register = ParseFactory.register(NUser_4.class);
	String str1 = "1|#|张三|#|1901-01-01|#|2|#|李四|#|1902-01-01|#|3|#|王五|#|1903-01-01";
	String[] splitAndChecked = register.splitAndChecked(str1);
	List<NUser_4> parseList = register.parseList(splitAndChecked);
	for (NUser_4 nUser_4 : parseList) {
		System.out.println(nUser_4);
		Assert.assertNotEquals(nUser_4.getId(), 				null	);
		Assert.assertNotEquals(nUser_4.getName(), 				null	);
		Assert.assertNotEquals(nUser_4.getBrithday(), 			null	);
	}	
}
```
> 4.spring支持



使用方法同上面的
	
## 三、解析文本

适用于普通的小文本解析。

```java
public void test() throws IOException {
	// 注册类的解析器，自动返回一个解析器
	DataConfigProcess<User> dataConfigProcess = ParseFactory.register(User.class);
	// 获得类的解析器
	//DataConfigProcess<User> dataConfigProcess = ParseFactory.getMapper(User.class);
	String filePath = "src/test/java/data/bb.txt";
	// 读取小文件
	TextFileReader reader = new TextFileReader(filePath, dataConfigProcess.charsetName());
	// 获得所有行
	List<String> readLines = reader.readLines();
	// 遍历解析
	for (String str : readLines) {
		try {
			User user = dataConfigProcess.parse(str);
			System.out.println(user);
		} catch (DataParseException e) {
			e.printStackTrace();
		}
	}
	// 直接解析全部数据
	try {
		List<User> parseList = dataConfigProcess.parseList(readLines);
		System.out.println(parseList);
	} catch (DataParseException e) {
		e.printStackTrace();
	}
	// 关闭资源
	reader.close();
}	
```


# 第二章 实现原理

 [实现原理](https://gitee.com/zx19890628/dataParse/blob/master/%E6%BA%90%E7%A0%81%E5%88%86%E6%9E%90.md)



# 第三章 性能
 [性能](https://gitee.com/zx19890628/dataParse/blob/master/%E6%80%A7%E8%83%BD%E6%B5%8B%E8%AF%95.md)


# 第四章 谁在使用

![输入图片说明](https://images.gitee.com/uploads/images/2019/0302/223533_275a81e0_635176.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0302/223137_dd84cc7a_635176.png "屏幕截图.png")







